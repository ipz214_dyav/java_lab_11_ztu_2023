package com.education.ua;

public class Product {
    private int id;
    private String name;
    private String count;

    public Product(String name, String count) {
        this.name = name;
        this.count = count;
    }

    public Product() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCount() {
        return count;
    }

    public void setCount(String count) {
        this.count = count;
    }
}
