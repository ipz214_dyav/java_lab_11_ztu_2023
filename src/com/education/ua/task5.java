package com.education.ua;

import java.sql.*;

public class task5 {
    public static void main(String[] args) throws SQLException {
        Connection connection = SQLDatabaseConnection.connect();
        connection.setAutoCommit(false);
        Statement statement = connection.createStatement();

        //   (   )
        String insertProduct1SQL = "INSERT INTO product (name, count ) VALUES (' 1', 10)";
        String insertProduct2SQL = "INSERT INTO product (name, count) VALUES (' 2' , 20"; //  

        statement.executeUpdate(insertProduct1SQL);

        //       
        Savepoint savepoint = connection.setSavepoint("__1");
        try {
            statement.executeUpdate(insertProduct2SQL); //      
        } catch (SQLException e) {
            System.out.println("    . ³ .");
            connection.rollback(savepoint); // ³   
        }

        //  ,       
        String selectProductsSQL = "SELECT * FROM product";
        ResultSet resultSet = statement.executeQuery(selectProductsSQL);

        System.out.println("     :");
        while (resultSet.next()) {
            int productId = resultSet.getInt("id");
            String productName = resultSet.getString("name");
            double productCount = resultSet.getDouble("count");

            System.out.println("ID: " + productId + ", Name: " + productName + ", Count: " + productCount);
        }

        // ϳ 
        connection.commit();

        //  
        resultSet.close();
        statement.close();
        connection.close();
    }
}
