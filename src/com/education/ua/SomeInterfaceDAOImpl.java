package com.education.ua;

import java.sql.*;
import java.util.ArrayList;
import java.util.List;

public class SomeInterfaceDAOImpl implements SomeInterfaceDAO<Product> {
    private Connection connection;

    public SomeInterfaceDAOImpl(Connection connection) {
        this.connection = connection;
    }

    @Override
    public Product getById(int id) throws SQLException {
        String selectProductsSQL = "SELECT * FROM lab11.product WHERE id=(?)";
        Product product = new Product();
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(selectProductsSQL);
            preparedStatement.setInt(1, id);
            ResultSet set = preparedStatement.executeQuery();
            if (set.next()) {
                product.setId(set.getInt("id"));
                product.setName(set.getString("name"));
                product.setCount(set.getString("count"));
            }
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return product;
    }

    @Override
    public List<Product> getAll() throws SQLException {
        Statement statement = connection.createStatement();
        List<Product> productList = new ArrayList<>();
        String selectProductsSQL = "SELECT * FROM lab11.product";

        try {
            ResultSet set = statement.executeQuery(selectProductsSQL);
            while (set.next()) {
                Product product = new Product();
                product.setId(set.getInt("id"));
                product.setName(set.getString("name"));
                product.setCount(set.getString("count"));
                productList.add(product);
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
        return productList;
    }

    @Override
    public void insert(Product obj) {
        String insertProduct1SQL = "INSERT INTO lab11.product (name, count ) VALUES (?, ?)";

        try {
            PreparedStatement preparedStatement = connection.prepareStatement(insertProduct1SQL);
            preparedStatement.setString(1, obj.getName());
            preparedStatement.setString(2, obj.getCount());

            int i = preparedStatement.executeUpdate();
            System.out.println("  ");
            System.out.println("ʳ :" + i);

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void update(int id ,Product obj) {
        String sql = "UPDATE lab11.product SET name = ?, count = ? WHERE id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setString(1, obj.getName());
            preparedStatement.setString(2, obj.getCount());
            preparedStatement.setInt(3, id);

            int rowsUpdated = preparedStatement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("  ");
            } else {
                System.out.println("   id   ,     ");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void delete(int id) {
        String sql = "DELETE FROM lab11.product  WHERE id = ?";
        try {
            PreparedStatement preparedStatement = connection.prepareStatement(sql);
            preparedStatement.setInt(1, id);

            int rowsUpdated = preparedStatement.executeUpdate();
            if (rowsUpdated > 0) {
                System.out.println("  ");
            } else {
                System.out.println("   id   ,     ");
            }

        } catch (SQLException e) {
            e.printStackTrace();
        }
    }
}
