package com.education.ua;

import java.sql.Connection;
import java.sql.SQLException;
import java.util.List;

public class task6 {
    public static void main(String[] args) throws SQLException {
        Connection connection = SQLDatabaseConnection.connect();
        SomeInterfaceDAOImpl dao = new SomeInterfaceDAOImpl(connection);

//        dao.insert(new Product("","20"));
//        dao.insert(new Product("2","30"));
//        dao.insert(new Product("3","40"));

//        List<Product> all = dao.getAll();
//        all.stream().map(Product::getName).forEach(System.out::println);

//        Product byId = dao.getById(25);
//        System.out.println(byId.getId() + byId.getName() + byId.getCount());

//        dao.update(25,new Product(" ", "345"));
//
        dao.delete(25);

    }
}

