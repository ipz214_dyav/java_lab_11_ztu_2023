package com.education.ua;

import java.sql.*;
import java.util.Locale;
import java.util.ResourceBundle;

public class SQLDatabaseConnection {
    public static Connection connect() throws SQLException {
        ResourceBundle rb = ResourceBundle.getBundle("connectDB");
        String name = rb.getString("user.name");
        String pass = rb.getString("user.password");

        return DriverManager.getConnection(
                "jdbc:mariadb://localhost:3306/lab11", name, pass);
    }
}
